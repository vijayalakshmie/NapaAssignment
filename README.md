# Napa Assignment

This is the programming assignment to Napa for the role of Software Developer Summer Trainee.

## Environment
1. Operating System: Windows 10 64-bit
2. Programming Language: C#, .Net Core 2.0
3. IDE: Visual Studio 2017

## How to read the solution
1. Open the solution "NapaAssignment.sln" from the NapaAssignment folder through Visual Studio.
2. The solution contains three projects:
    1. HexagonalGridRoute.Console -> C# console application
    2. HexagonalGridRoute.Lib -> class library as base project for console & test projects
    3. HexagonalGridRoute.Tests -> unit test project

## How to run the solution
1. Open the solution "NapaAssignment.sln" from the NapaAssignment folder through Visual Studio.
2. Run the application (Ctrl + F5)

## Results

Example:

```
Number of steps: 818
```